#!/usr/bin/env bash

set -euo pipefail

color_cmd='[34m'
color_info='[35m'
color_error='[31m'
color_reset='[m'

run()
{
	# $ run echo "hello world"
	# works for commands with arguments
	# does not work for pipes, redirects and stuff
	echo "$color_cmd\$ $@$color_reset" >/dev/tty
	"$@"
}

info()
{
	echo "$color_info$@$color_reset" >/dev/tty
}

error()
{
	echo "$color_error$@$color_reset" >/dev/tty
}


ask_for_input()
{
	local var prompt
	var="$1"
	prompt="$2"
	read -p "$color_info$prompt:$color_reset " -r "$var"
}

ask_for_password()
{
	local var prompt bak
	var="$1"
	prompt="$2"
	while true; do
		read -s -p "$color_info$prompt:$color_reset " -r "$var"
		echo
		read -s -p "${color_info}please repeat password:$color_reset " -r bak
		echo
		if [ "${!var}" = "$bak" ]; then
			return
		fi
		error "inputs do not match, please try again"
	done
}

ask_yes_no() {
	local ans
	while true; do
		read -p "$1 [Y/n] " -r ans
		if [ "$ans" = "y" -o "$ans" = "Y" ]; then
			return 0
		elif [ "$ans" = "n" -o "$ans" = "N" ]; then
			return 1
		elif [ "$ans" = "" ]; then
			return 0
		else
			echo "Invalid input '$ans'."
		fi
	done
}
