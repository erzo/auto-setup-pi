# /boot/custom.toml
[firstboot](https://github.com/RPi-Distro/raspberrypi-sys-mods/blob/master/usr/lib/raspberrypi-sys-mods/firstboot) reads `/boot/custom.toml` if it exists but I have failed to find any documentation for it.
Therefore I have gathered all allowed sections and keys from the [source code](https://github.com/RPi-Distro/raspberrypi-sys-mods/blob/master/usr/lib/raspberrypi-sys-mods/init_config):

- `system`
	- `hostname`
- `user`
	- `name`
	- `password`
	- `password_encrypted`
- `ssh`
	- `ssh_import_id`
	- `enabled`
	- `password_authentication`
	- `authorized_keys`
- `wlan`
	- `ssid`
	- `password`
	- `password_encrypted`
	- `hidden`
	- `country`
- `locale`
	- `keymap`
	- `timezone`
