#!/usr/bin/env bash

# ------- strict mode -------

set -euo pipefail


# ------- settings -------

# SD card / device to write to
dev='/dev/mmcblk0'

#https://stackoverflow.com/a/246128
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

. "$SCRIPT_DIR/ui.sh"


# ------- utils -------

is_installed() { which "$1" >/dev/null; }
is_in_group() { id -nG "`whoami`" | grep -qw "$1"; }


# ------- check sd card -------

check_sd_card()
{
	if ! lsblk "$dev" &>/dev/null; then
		error "Please insert the SD card to write to in $dev or change the value of \$dev to point to the device which you want to write to."
		exit 1
	fi
	if lsblk -o path,mountpoint -P "$dev" | grep -vq 'MOUNTPOINT=""'; then
		error "SD card is mounted, please unmount it first. Are you sure you want to write to the device in $dev?"
		lsblk "$dev"
		exit 1
	fi
}


# ------- download, check, extract -------

download()
{
	local url fnhtml imgurl xzimg img expectedchk realchk
	url='https://www.raspberrypi.com/software/operating-systems/'
	fnhtml="$(mktemp)"
	info "get list of latest download links"
	if ! run curl "$url" -o "$fnhtml"; then
		img="$(`which ls` -1 *.img | tail -1)"
		if [ "$img" = '' ]; then
			error "no internet connection and no previously img file found"
			exit 1
		else
			info "no internet connection, using previously downloaded img file"
			echo "$img"
			return
		fi
	fi
	imgurl=$(extract_img_url "$fnhtml")
	xzimg="${imgurl##*/}"
	img="${xzimg%.xz}"
	if [ -f "$img" ]; then
		echo "$img"
		return
	fi
	info "download $img"
	run curl "$imgurl" -o "$xzimg"
	expectedchk="$(extract_sha256 "$fnhtml" "$imgurl")"
	realchk="$(sha256sum "${xzimg}" | sed 's/ .*//')"
	if [ "$expectedchk" != "$realchk" ]; then
		error "sha256 checksum does not match (expected $expectedchk, got $realchk)"
		exit 1
	fi
	unxz "$xzimg"
	echo "$img"
}

extract_img_url()
{
	local fnhtml
	fnhtml="$1"
	python3 -c '
import sys
from html.parser import HTMLParser

class MyHTMLParser(HTMLParser):

	def handle_starttag(self, tag, attrs):
		attrs = dict(attrs)
		if tag == "a" and attrs.get("href", "").endswith("arm64-lite.img.xz"):
			print(attrs["href"])
			sys.exit(0)

	def handle_endtag(self, tag):
		pass

	def handle_data(self, data):
		pass

with open("'"$fnhtml"'") as f:
	parser = MyHTMLParser()
	parser.feed(f.read())

sys.exit(1)
'
}

extract_sha256()
{
	local fnhtml imgurl
	fnhtml="$1"
	imgurl="$2"
	python3 -c '
import sys
from html.parser import HTMLParser

imgurl="'"$imgurl"'"

class MyHTMLParser(HTMLParser):

	ischk = False

	def handle_starttag(self, tag, attrs):
		attrs = dict(attrs)
		if attrs.get("class", "").endswith("sha256-value"):
			self.ischk = True
		elif tag == "a" and attrs.get("href", "") == imgurl:
			print(self.chk)
			sys.exit(0)

	def handle_endtag(self, tag):
		self.ischk = False

	def handle_data(self, data):
		if self.ischk:
			self.chk = data

with open("'"$fnhtml"'") as f:
	parser = MyHTMLParser()
	parser.feed(f.read())

sys.exit(1)
'
}


# ------- write to sd card -------

write_to_sd_card()
{
	local fnimg boot root script cwd
	fnimg="$1"
	boot="$2"
	root="$3"

	info "write $fnimg to SD card"

	script="$(mktemp)"
	cwd="$(pwd)"
	cat >"$script" <<EOF
	set -euo pipefail
	cd "$cwd"
	dd if="$fnimg" of="$dev" status=progress
	sync
	partprobe

	mount -o X-mount.idmap='u:0:`id -u`:1 g:0:`id -g`:1' '${dev}p1' '$boot'
	mount -o X-mount.idmap='u:0:`id -u`:1 g:0:`id -g`:1' '${dev}p2' '$root'

	su `whoami` -c 'bash "$SCRIPT_DIR/modify-sd.sh" "$boot" "$root"'

	sync
	umount "$root"
	umount "$boot"
EOF
	cat "$script"
	if is_installed sudo && is_in_group sudo; then
		sudo bash "$script"
	else
		echo "please enter the root password"
		su - -c "bash '$script'"
	fi

	sleep 1
	rm "$script"
}


# ------- main -------

main()
{
	local img tmp boot root
	check_sd_card
	img="$(download)"

	tmp="$(mktemp -d)"
	boot="$tmp/boot"
	root="$tmp/root"
	mkdir "$boot"
	mkdir "$root"
	write_to_sd_card "$img" "$boot" "$root"
}

main
