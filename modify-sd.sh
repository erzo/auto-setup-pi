#!/usr/bin/env bash

set -euo pipefail

#https://stackoverflow.com/a/246128
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

. "$SCRIPT_DIR/ui.sh"


modifysd()
{
	local boot root config init initonpi
	boot="$1"
	root="$2"
	config="$boot/custom.toml"
	initonpi="/boot/custom-init.sh"
	init="$boot/${initonpi#/boot/}"

	sed -i '/^main$/ a \
\
whiptail --infobox "run '"$initonpi"'" 20 60 \
bash '"'$initonpi'" "$root/usr/lib/raspberrypi-sys-mods/firstboot"
	echo >>"$init" '#!/usr/bin/env bash'
	#echo >>"$init" 'set -euo pipefail'
	echo >>"$init" ''

	update "$init"
	renameuser "$config"
	setlocale "$config"
	configuressh "$boot" "$config" "$init"
	showip "$init"
	sudohardening "$init"

	echo >>"$init" "rm '$initonpi'"
}

update()
{
	local init
	init="$1"

	echo >>"$init" 'apt-get update -y'
	echo >>"$init" 'apt-get upgrade -y'
	echo >>"$init" 'apt-get update -y'
	echo >>"$init" 'apt-get dist-upgrade -y'
	echo >>"$init" 'apt-get autoremove -y'
	echo >>"$init" ''

	echo >>"$init" 'apt-get install -y vim'
	echo >>"$init" 'apt-get install -y fd-find'

	echo "I have written update commands to $init"
}

renameuser()
{
	local config name pass encpass
	config="$1"

	ask_for_input name "user name"
	if [ "$name"  = "" ]; then
		return
	fi

	ask_for_password pass "user password"
	# https://github.com/RPi-Distro/raspberrypi-sys-mods/blob/master/usr/lib/raspberrypi-sys-mods/init_config uses crypt but that is deprecated
	# https://serverfault.com/a/1109321 uses a different standard library, but that function is deprecated, too
	# https://unix.stackexchange.com/a/693856 looked good, but chat GPT claims "No, the hmac module is not a suitable replacement for password hashing."
	encpass="$(python3 -c "import crypt; print(crypt.crypt('"$pass"', crypt.mksalt(crypt.METHOD_SHA512)))")"

	echo >>"$config" '[user]'
	echo >>"$config" "name = \"$name\""
	echo >>"$config" "password = \"$encpass\""
	echo >>"$config" "password_encrypted = true"
	echo >>"$config" ""

	echo "I have written the user name and encrypted password to $config"
}

setlocale()
{
	local config kb _kb tz _tz
	config="$1"

	kb="$(sed -n 's/KEYMAP=//p' /etc/vconsole.conf 2>/dev/null)"
	ask_for_input _kb "keyboard layout (${kb:-e.g. us})"
	kb="${_kb:-$kb}"

	tz="$(realpath /etc/localtime | sed 's:/usr/share/zoneinfo/::')"
	ask_for_input _tz "time zone (${tz:-e.g. Europe/London})"
	tz="${_tz:-$tz}"

	echo >>"$config" '[locale]'
	if [ "$kb" != "" ]; then
		echo >>"$config" "keymap = \"$kb\""
	fi
	if [ "$tz" != "" ]; then
		echo >>"$config" "timezone = \"$tz\""
	fi
	echo >>"$config" ""

	echo "I have written keyboard layout and time zone to $config"
}

configuressh()
{
	local boot config init
	boot="$1"
	config="$2"
	init="$3"

	if ask_yes_no "enable ssh?"; then
		#echo >>"$config" '[ssh]'
		#echo >>"$config" "enabled = true"
		#echo >>"$config" "password_authentication = true"
		#echo >>"$config" ""
		#echo "I have written ssh settings to $config"
		touch "$boot/ssh"   #https://pimylifeup.com/raspberry-pi-enable-ssh-without-monitor/
		echo >>"$init" 'setssh() { sed -Ei "s/^[# ]*($1) (.*)/\\1 $2  # changed by setup script from \\2/"  "/etc/ssh/sshd_config"; }'
		echo >>"$init" 'setssh PasswordAuthentication yes'
		echo >>"$init" 'setssh PermitEmptyPasswords no'
		echo >>"$init" 'setssh PermitRootLogin no'
		echo >>"$init" 'setssh MaxAuthTries 3   # default: 6'
		echo >>"$init" 'setssh MaxSessions 5    # default: 10'
		echo >>"$init" 'setssh X11Forwarding no'
		echo >>"$init" 'systemctl enable sshd'
		echo >>"$init" 'systemctl restart sshd'
		echo >>"$init" ''
		echo "I have written ssh configuration commands to $init and created empty file $boot/ssh"
	fi

	local hostname
	ask_for_input hostname "hostname"
	if [ "$hostname" != "" ]; then
		echo >>"$config" '[system]'
		echo >>"$config" "hostname = \"$hostname\""
		echo >>"$config" ""
	fi
}

showip()
{
	local init
	init="$1"

	#https://askubuntu.com/a/797600
	echo >>"$init" "if ! grep -q eth0 /etc/issue; then"
	echo >>"$init" "    echo >>/etc/issue 'My ip address is \\4{eth0}'"
	echo >>"$init" "fi"
	echo >>"$init" ""
}

sudohardening()
{
	local init sudoers
	init="$1"
	sudoers=/etc/sudoers

	echo >>"$init" "lnno=\"\$(grep -n Defaults '$sudoers' | tail -1 | sed -E 's/^([0-9]+):.*/\\1/')\""
	echo >>"$init" "sed -i \"\$lnno\"' a\\
Defaults	env_reset              # added by setup script\\
Defaults	timestamp_timeout=0    # added by setup script
' '$sudoers'"
	echo >>"$init" 'mv /etc/sudoers.d/010_pi-nopasswd /etc/sudoers.d/.010_pi-nopasswd'
	echo >>"$init" ''

	echo "I have written sudo configuration commands to $init"
}

modifysd "$@"
