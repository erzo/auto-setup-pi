This is a script to automate setting up Raspberry Pis.

This script:
- downloads the latest version of the [Raspberry Pi OS](https://www.raspberrypi.com/software/operating-systems/) 64-bit lite if it has not been downloaded already.
- verifies that the downloaded file is correct via the SHA256 file integrity hash.
- unpacks the downloaded xz file.
- writes the img file to the SD card.
- mounts the two partitions of the SD card to do some configuration and unmounts them again.


This script does the following configuration:
- asks for a user name and password.
- tries to figure out keyboard layout and time zone based on the settings of the machine where this script is executed.
- enable ssh via passwords, disables root login via ssh, empty passwords and X11 forwarding.
- print the ipv4 address at the login prompt.
- require a password when using sudo.


How the configuration works:
- This script adds three files to the boot partition (partition 1) of the SD card: `ssh`, `custom.toml` and `custom-init.sh`.
- `ssh` is just an empty file which causes ssh to be enabled, see the [documentation of the boot folder](https://www.raspberrypi.com/documentation/computers/configuration.html#the-boot-folder).
- I have failed to find any documentation for `custom.toml`.
  Therefore I have extracted the allowed sections and keys from the source code and listed them [here](custom_toml.md).
- `custom.toml` is read by [/usr/lib/raspberrypi-sys-mods/init_config](https://github.com/RPi-Distro/raspberrypi-sys-mods/blob/master/usr/lib/raspberrypi-sys-mods/init_config)
  which is called by [/usr/lib/raspberrypi-sys-mods/firstboot](https://github.com/RPi-Distro/raspberrypi-sys-mods/blob/master/usr/lib/raspberrypi-sys-mods/firstboot).
  The `/usr/lib` directory is located on the root partition which is partition 2.
- Before the first boot the file `cmdline.txt` on the boot partition (partition 1) contains the command line argument `init=/usr/lib/raspberrypi-sys-mods/firstboot` which causes `firstboot` to be executed on the first boot.
  `firstboot` removes this argument from `cmdline.txt` so that it will not be called again.
- This script modifies `firstboot` to call `custom-init.sh` as last step before rebooting.
- This script uses the `X-mount.idmap` option of the `mount` command to run as many steps as possible as normal user.
- The last action of `custom-init.sh` is to remove itself.


Usage:
- Insert the SD card into a computer.
- Run main.sh in bash.
    - Wait until the SD card image has been downloaded (only the first time you run this script and when a new OS version has been released).
    - Insert the password to get root priviliges for writing to the SD card.
    - Wait until the downloaded image has been written to the SD card.
    - Answer the questions of the script.
- Plug the SD card into the raspberry pi.
- Connect the raspberry pi to a network cable and power.
- Log in via ssh and run `sudo /boot/custom-init.sh`.
  (This last step is not intended to be necessary.
   /boot/custom-init.sh is called by firstboot but
   it fails immediately with countless "read only file system" errors.
   But after the raspberry pi has rebooted automatically custom-init.sh runs just fine.)


Bugs:
- /boot/custom-init.sh is written and called at the end of firstboot but fails immediately with countless "read only file system" errors.
  As a workaround run it manually after the raspberry pi has rebooted automatically.
- The hostname appears in the prompt of the shell but not in the network.
  As a workaround check the connected devices in your router and use the hostname/ip address listed there.
